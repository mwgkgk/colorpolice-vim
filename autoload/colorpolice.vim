function! colorpolice#sync(...)
    if a:0 == 0
        let scheme = g:colors_name
    elseif a:0 == 1
        if strlen(a:1) == 0
            let scheme = g:colors_name
        else
            let scheme = a:1
        endif
    endif
    let servers = split(serverlist(), '\n')
    for serv in servers
        let res = remote_send(serv, ':ColorPolice ' . scheme . '<CR>')
    endfor
endfunc

function! colorpolice#select(...) abort
    if exists('g:colors_name')
        let l:colors_name = get(a:, 1, g:colors_name)
        if empty(l:colors_name)
            let l:colors_name = g:colors_name
        endif
    else
        let l:colors_name = get(a:, 1, 'default')
    endif

    let l:fpath = '/roster/' . fnameescape(l:colors_name) . '.vim'
    if !empty(globpath(&runtimepath, l:fpath))
        exe 'runtime ' . l:fpath
    else
        exe 'colorscheme ' . l:colors_name
    endif

    hi EasyMotionTarget ctermbg=none ctermfg=202
    hi EasyMotionTarget2First ctermbg=none ctermfg=202
    hi EasyMotionTarget2Second ctermbg=none ctermfg=208

    " Fix markdown
    hi clear markdownError
    hi link markdownError Normal
    hi clear markdownCode
    hi link markdownCode Normal
    hi clear markdownCodeBlock
    hi link markdownCodeBlock Normal
    hi clear markdownItalic
    hi link markdownItalic Normal

    " Fix term status
    hi clear StatusLineTerm
    hi clear StatusLineTermNC
    hi link StatusLineTerm StatusLine
    hi link StatusLineTermNC StatusLineNC

    " Default to dark term
    " hi Terminal cterm=none ctermbg=234 ctermfg=11
    " Freefall
    " hi Terminal ctermbg=233 ctermfg=254 cterm=none
    " Net-yellow
    hi Terminal ctermbg=233 ctermfg=214 cterm=none
    " fadedvision-dark
    " hi Terminal ctermbg=232 ctermfg=151 cterm=none
    " Chernozem
    " hi Terminal ctermbg=232 ctermfg=131 cterm=none
    " alien-nes
    " hi Terminal ctermbg=233 ctermfg=85 cterm=none
    " druid
    " hi Terminal ctermbg=233 ctermfg=106

    " Should work with anything?
    " hi Terminal cterm=none ctermbg=none ctermfg=none
endfunction

function! colorpolice#complete(arglead, cmdline, cursorpos) abort
    let l:colors = getcompletion(a:arglead, 'color', 1)

    " Using a gorgeous https://vi.stackexchange.com/a/9510
    let l:rtps = split(&runtimepath, ',')

    for l:rtp in l:rtps
        " The idea is to find which runtimepath contains our roster. However
        " the roster folder alone is not a good enough marker: we'll select an
        " rtp that contains plugin/colorpolice.vim:
        if !empty(glob(l:rtp . 'plugin/colorpolice.vim'))
            let l:colors += glob(l:rtp . 'roster/' . a:arglead . '*.vim', 0, 1)
                        \ ->map("fnamemodify(v:val, ':t:r')")
        endif
    endfor

    return uniq(sort(l:colors))
endfunction
