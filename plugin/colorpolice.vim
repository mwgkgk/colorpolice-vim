" Select colorscheme for the current Vim instance:
command! -nargs=? -complete=customlist,colorpolice#complete ColorPolice call colorpolice#select(<q-args>)

" Select colorscheme for all vim servers in serverlist():
command! -nargs=? -complete=customlist,colorpolice#complete ColorPoliceSync call colorpolice#sync(<q-args>)
