silent! colorscheme 256-jungle

hi TabLineFill ctermfg=233
hi TabLineSel ctermbg=234 ctermfg=253 cterm=none
hi TabLine ctermbg=232

hi CursorColumn ctermbg=233
hi CursorLineNr ctermbg=233 ctermfg=255 cterm=none

hi NonText ctermfg=239

hi StatusLine ctermbg=235
hi StatusLineNC ctermbg=235
hi VertSplit ctermbg=235 ctermfg=235 cterm=none

hi Folded ctermbg=235 ctermfg=95 cterm=none

hi Search ctermfg=none ctermbg=239

hi Comment ctermfg=131
