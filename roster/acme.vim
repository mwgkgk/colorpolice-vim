silent! colorscheme acme

hi Statement cterm=none
hi Identifier cterm=none
hi Special cterm=none
hi Constant cterm=none
hi Comment cterm=none ctermfg=102
hi Folded cterm=none ctermbg=230
hi SignColumn ctermbg=230
hi StatusLine ctermbg=194
hi VertSplit ctermbg=194
hi Type cterm=none
hi PreProc cterm=none
hi Title cterm=none ctermfg=232
hi NonText ctermfg=248

hi Search ctermbg=158
hi IncSearch ctermbg=222 cterm=none

hi LineNr cterm=none
hi CursorLine cterm=none ctermbg=194 ctermfg=none
hi CursorLineNr cterm=none ctermbg=194 ctermfg=232
hi CursorColumn cterm=none ctermbg=194 ctermfg=none

hi ColorColumn ctermbg=224

hi Pmenu ctermbg=224

hi ErrorMsg ctermbg=209 ctermfg=232

hi Todo ctermbg=none
