let g:alduin_Shout_Dragon_Aspect = 1 " Almost Black
" let g:alduin_Shout_Become_Ethereal = 1 " Black
let g:alduin_Shout_Fire_Breath = 0
let g:alduin_Shout_Animal_Allegiance = 1 " No string bg

silent! colorscheme alduin

hi SpecialComment cterm=none
