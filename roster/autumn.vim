silent! colorscheme autumn

hi Statement ctermfg=70
hi Special ctermfg=111
hi Identifier ctermfg=133

hi Title ctermfg=131

hi CursorLine cterm=none ctermbg=255
hi CursorColumn cterm=none ctermbg=255
hi ColorColumn cterm=none ctermbg=255

hi LineNr ctermbg=15 ctermfg=250
hi CursorLineNr cterm=none
hi Folded ctermbg=254 ctermfg=241
hi SignColumn ctermbg=15 ctermfg=172

hi VertSplit ctermbg=187 ctermfg=250
hi StatusLineNC ctermbg=187 ctermfg=244
hi StatusLine ctermbg=187 ctermfg=237

hi Search ctermbg=222 ctermfg=237
hi IncSearch ctermbg=216 ctermfg=237

hi ModeMsg ctermfg=237 ctermbg=222

hi Error cterm=none ctermbg=none ctermfg=88 cterm=bold
hi Todo cterm=none ctermbg=none ctermfg=173 cterm=bold

hi MatchParen ctermbg=175 ctermfg=230 cterm=none
