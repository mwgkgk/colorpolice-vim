colo base16-atelierseaside

hi StatusLine guifg=#FFFFFF
hi Visual guifg=#FFFFFF

hi Search guifg=#3F3F3F guibg=#F4A1C3

" WIP
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Comment'],
            \ 'bg':      ['bg', 'Comment'],
            \ 'hl':      ['fg', 'Statement'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Comment'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Normal'] }
