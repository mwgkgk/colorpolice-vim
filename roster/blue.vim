silent! colorscheme blue

" This one uses a pleasant #3465A4 from ~/.Xresources as color4 for Normal bg.
" Also this explains why the tones don't really fit, or at least = challenging.

hi Normal ctermfg=222
hi Title ctermfg=111
hi PreProc ctermfg=119
hi Constant ctermfg=143
hi Identifier ctermfg=202

hi Search ctermfg=4 ctermbg=222
hi Todo ctermfg=173 ctermbg=none cterm=none

hi CursorLine cterm=none ctermfg=none ctermbg=95

" hi ColorColumn ctermbg=59
" hi ColorColumn ctermbg=239
" hi ColorColumn ctermbg=25
" hi ColorColumn ctermfg=none ctermbg=235
hi ColorColumn ctermfg=none ctermbg=none

hi LineNr ctermfg=138 ctermbg=none
hi CursorLineNr ctermfg=216 ctermbg=95
hi SignColumn ctermfg=166 ctermbg=none

hi Folded ctermfg=209 ctermbg=none
hi FoldColumn ctermfg=209 ctermbg=none

hi VertSplit ctermfg=238 ctermbg=none

hi TabLineFill ctermfg=238
hi TabLine ctermbg=238 ctermfg=222  cterm=none
hi TabLineSel ctermfg=222

hi StatusLine ctermfg=238 ctermbg=222
hi StatusLineNC ctermfg=238 ctermbg=244

hi Visual ctermfg=none ctermbg=204
