silent! colorscheme calmar256-dark

hi TabLineSel ctermfg=158 ctermbg=16
hi TabLine ctermfg=247 ctermbg=16
hi NonText ctermfg=238
hi SpecialKey ctermfg=238
