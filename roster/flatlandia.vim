silent! colorscheme flatlandia

hi NonText ctermbg=none ctermfg=241
hi SpecialKey ctermbg=none ctermfg=137

hi LineNr ctermbg=235 ctermfg=243
hi EndOfBuffer ctermfg=244 ctermbg=236

hi StatusLine cterm=none
hi StatusLineNC ctermfg=247 ctermbg=238

hi SignColumn ctermbg=236 ctermfg=248
