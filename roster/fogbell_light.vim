silent! colorscheme fogbell_light

hi CursorColumn ctermbg=250
hi CursorLine ctermbg=250 cterm=none
hi CursorLineNr cterm=none ctermbg=250
hi ColorColumn ctermbg=250

hi StatusLine ctermbg=245 ctermfg=255 cterm=none
hi StatusLineNC ctermbg=245 ctermfg=252 cterm=none
hi VertSplit ctermfg=252 ctermbg=245

hi Search ctermbg=255 ctermfg=233
hi Visual ctermbg=249
hi MatchParen ctermbg=249

hi LineNr ctermfg=233 ctermbg=249
hi CursorLineNr ctermfg=233 ctermbg=246
hi SignColumn ctermfg=233 ctermbg=none

hi TabLine cterm=none ctermbg=247
