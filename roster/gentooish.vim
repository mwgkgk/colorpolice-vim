silent! colorscheme gentooish

" Not set in the original
let g:colors_name = 'gentooish'

hi ColorColumn ctermfg=none ctermbg=235
hi CursorLine cterm=none ctermfg=none ctermbg=235

hi LineNr ctermfg=241
hi CursorLineNr ctermfg=133

hi VertSplit ctermfg=236 ctermbg=none

hi String ctermbg=none
hi SpecialKey ctermfg=237

hi TabLineFill ctermfg=235
hi TabLine ctermbg=235 ctermfg=241 cterm=none
hi TabLineSel ctermbg=235 ctermfg=133
