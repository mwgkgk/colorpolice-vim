silent! colorscheme github

hi SignColumn ctermbg=231 ctermfg=172
hi LineNr ctermbg=231 ctermfg=250

hi StatusLine ctermbg=255 cterm=none ctermfg=204
hi StatusLineNC cterm=none

hi VertSplit ctermbg=255 ctermfg=253 cterm=none
hi TabLineFill ctermbg=255
hi TabLine ctermbg=255
