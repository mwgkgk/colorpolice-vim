set background=dark

let g:gruvbox_contrast_dark='dark'

silent! colorscheme gruvbox
let g:colors_name = 'gruvbox-dark'

hi Pmenu           ctermfg=none     ctermbg=237
hi PmenuSel        ctermfg=222     ctermbg=240  cterm=None
hi PmenuSbar       ctermfg=231     ctermbg=244

hi SignColumn ctermbg=235
hi GitGutterAdd ctermbg=235 ctermfg=130
hi GitGutterDelete ctermbg=235 ctermfg=122
hi GitGutterChange ctermbg=235 ctermfg=122
hi GitGutterChangeDeleteLine ctermbg=235 ctermfg=122
