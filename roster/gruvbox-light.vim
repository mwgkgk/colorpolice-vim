set background=light

silent! colorscheme gruvbox
let g:colors_name = 'gruvbox-light'

" syntax off
hi Pmenu           ctermfg=95     ctermbg=230
hi PmenuSel        ctermfg=fg     ctermbg=222  cterm=None
hi PmenuSbar       ctermfg=231    ctermbg=244

hi SignColumn ctermbg=none

hi GitGutterAdd          ctermfg=28    ctermbg=none
hi GitGutterChange       ctermfg=203   ctermbg=none
hi GitGutterDelete       ctermfg=196   ctermbg=none
hi GitGutterChangeDelete ctermfg=196   ctermbg=none

hi CursorLine ctermbg=230
