set background=dark

silent! colorscheme hemisu
let g:colors_name = 'hemisu-dark'

hi Normal ctermbg=none
hi CursorLine ctermbg=235
hi ColorColumn ctermbg=235
