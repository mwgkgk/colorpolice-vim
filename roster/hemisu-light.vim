set background=light

hi Normal ctermbg=none
hi CursorLine ctermbg=235
hi ColorColumn ctermbg=235

" This line has to be specifically here for this to work.
silent! colorscheme hemisu

let g:colors_name = 'hemisu-light'
