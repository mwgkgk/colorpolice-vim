set background=light
silent! colorscheme hybrid
let g:colors_name = 'hybrid-light'

hi Folded ctermfg=240 ctermbg=252

hi MatchParen cterm=none ctermbg=none ctermfg=63

hi SignColumn ctermbg=254 ctermfg=243

" FML
hi SignatureMarkText ctermbg=254 ctermfg=243
hi SignatureMarkerText ctermbg=254 ctermfg=243

hi GitGutterAdd ctermbg=254 ctermfg=243
hi GitGutterChange ctermbg=254 ctermfg=243
hi GitGutterDelete ctermbg=254 ctermfg=243
hi GitGutterChangeDelete ctermbg=254 ctermfg=243
