silent! colorscheme industry

hi Normal ctermbg=16 ctermfg=255

hi CursorLine cterm=none ctermbg=235
hi CursorColumn ctermbg=235
hi CursorLineNr cterm=none ctermbg=235

hi ColorColumn ctermbg=234
hi Folded ctermfg=242 ctermbg=234
hi LineNr ctermbg=16

hi Comment ctermfg=51

hi Title ctermfg=3

hi VertSplit ctermbg=234
hi StatusLine ctermbg=238
hi StatusLineNC ctermbg=234 ctermfg=242
