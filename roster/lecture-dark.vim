silent! colorscheme lecture
let g:colors_name = 'lecture-dark'

hi Normal ctermbg=234

hi StatusLine   ctermfg=137 ctermbg=233 cterm=bold
hi StatusLineNC ctermfg=240 ctermbg=233 cterm=none
hi VertSplit    ctermfg=237 ctermbg=233 cterm=none

hi LineNr       ctermfg=242 ctermbg=233 cterm=none
hi CursorLineNr ctermfg=137 ctermbg=235 cterm=none

hi CursorColumn ctermfg=none ctermbg=235 cterm=none
hi CursorLine   ctermfg=none ctermbg=235 cterm=none
hi ColorColumn  ctermbg=235

hi Folded       ctermbg=236  ctermfg=245
