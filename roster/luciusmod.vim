silent! colorscheme luciusmod

hi Normal ctermbg=235 " Is default value. Here to prove TabLineSel bg is same.

hi TabLineSel ctermbg=235 ctermfg=251 cterm=none
hi TabLineFill cterm=none
hi TabLine cterm=none

hi StatusLine cterm=none

hi ColorColumn ctermbg=236

hi Folded ctermbg=237
