set background=dark
silent! colorscheme lunaperche
let g:colors_name = 'lunaperche-dark'

hi GitGutterAdd ctermbg=none ctermfg=41
hi GitGutterDelete ctermbg=none ctermfg=198
hi GitGutterChange ctermbg=none ctermfg=198
hi GitGutterChangeDeleteLine ctermbg=none ctermfg=198
