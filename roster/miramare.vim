silent! colorscheme miramare

hi Comment ctermfg=79 cterm=none
hi Title ctermfg=121
hi String ctermfg=194
hi Function ctermfg=158
hi clojureFunc ctermfg=158
