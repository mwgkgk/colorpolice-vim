silent! colorscheme morning

hi CursorColumn ctermbg=255
hi CursorLine ctermbg=255 cterm=none
hi CursorLineNr cterm=none ctermbg=255
hi ColorColumn ctermbg=223

hi StatusLine ctermbg=253 ctermfg=174 cterm=none
hi StatusLineNC ctermbg=253 ctermfg=245 cterm=none
hi VertSplit ctermfg=253 ctermbg=245

hi TabLine cterm=none ctermbg=250

hi Folded ctermfg=245 ctermbg=252 cterm=none

hi Normal ctermfg=59
hi Constant ctermfg=132
hi Special ctermfg=35
hi NonText ctermfg=180
hi Comment ctermfg=138
hi String ctermfg=167
hi PreProc ctermfg=173
hi Type ctermfg=65
hi Statement cterm=none ctermfg=130
hi Identifier ctermfg=29
hi Todo ctermbg=223 ctermfg=none
hi Title ctermfg=138 cterm=bold

hi Search ctermbg=229
hi Visual ctermbg=253
hi MatchParen ctermbg=253
