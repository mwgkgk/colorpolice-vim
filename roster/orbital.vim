silent! colorscheme orbital

hi clear markdownCodeBlock

hi StatusLine ctermbg=233
hi StatusLineNC ctermfg=237 ctermbg=233

hi ColorColumn ctermbg=233 ctermfg=none
hi CursorLine ctermbg=235
