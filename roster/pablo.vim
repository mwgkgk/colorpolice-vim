" Mon Sep 23 11:57:23 UTC 2024

silent! colorscheme pablo

hi SignColumn ctermbg=16

hi SignatureMarkText     ctermbg=none
hi SignatureMarkerText   ctermbg=none

hi GitGutterAdd           ctermbg=none
hi GitGutterChange        ctermbg=none
hi GitGutterDelete        ctermbg=none
hi GitGutterChangeDelete  ctermbg=none
