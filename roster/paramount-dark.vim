set background=dark

silent! colorscheme paramount

let g:colors_name = 'paramount-dark'
