set background=light

silent! colorscheme paramount

let g:colors_name = 'paramount-light'
