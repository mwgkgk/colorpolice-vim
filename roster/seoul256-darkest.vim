" https://github.com/junegunn/seoul256.vim
"   Range:   233 (darkest) ~ 239 (lightest)
"   Default: 237

let g:seoul256_background = 233

silent! colorscheme seoul256
let g:colors_name = 'seoul256-darkest'

hi VertSplit ctermfg=236 ctermbg=233
hi StatusLine ctermfg=239 ctermbg=222 cterm=reverse
hi StatusLineNC ctermfg=239 ctermbg=235
hi Pmenu           ctermfg=fg     ctermbg=238    guifg=fg    guibg=#3A3A3A
hi PmenuSel        ctermfg=231    ctermbg=244    guifg=#FFFFFF    guibg=#808080
hi PmenuSbar       ctermfg=231    ctermbg=244    guifg=#FFFFFF    guibg=#808080

