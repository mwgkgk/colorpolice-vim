let g:seoul256_background = 255

silent! colorscheme seoul256-light
let g:colors_name = 'seoul256-light'

hi VertSplit cterm=none ctermbg=253 ctermfg=95
