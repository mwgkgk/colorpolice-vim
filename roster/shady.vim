silent! colorscheme shady

hi Normal ctermbg=234

hi Comment ctermfg=240 cterm=none
hi Folded  ctermfg=240 ctermbg=235

hi StatusLine   ctermfg=239 ctermbg=233
hi StatusLineNC ctermfg=239

hi CursorLine ctermbg=233
hi CursorColumn ctermbg=233
hi ColorColumn ctermbg=233
hi CursorLineNr ctermfg=244 ctermbg=235
hi LineNr      ctermfg=240

hi Label ctermfg=121
hi SpecialKey ctermfg=120

hi markdownCodeBlock cterm=none ctermfg=none ctermbg=none
