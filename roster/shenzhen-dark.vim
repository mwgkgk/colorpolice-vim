set background=dark
silent! colorscheme shenzhen
let g:colors_name = 'shenzhen-dark'

hi Normal ctermbg=232
hi StatusLine cterm=none ctermbg=234 ctermfg=222
hi StatusLineNC cterm=none ctermbg=234 ctermfg=238
hi StatusLineTerm cterm=none ctermbg=234 ctermfg=222
hi StatusLineTermNC cterm=none ctermbg=234 ctermfg=238
hi ColorColumn ctermbg=234
hi CursorLine ctermbg=235
hi CursorLineNr ctermbg=235
hi LineNr ctermbg=233
hi SignColumn ctermbg=233
hi GitGutterAdd ctermbg=233 ctermfg=130
hi GitGutterDelete ctermbg=233 ctermfg=122
hi GitGutterChange ctermbg=233 ctermfg=122
hi GitGutterChangeDeleteLine ctermbg=233 ctermfg=122
hi Folded ctermbg=234 ctermfg=243
