let g:solarized_termcolors=256
set background=light

silent! colorscheme solarized
let g:colors_name = 'solarized-light'
