silent! colorscheme summerfruit256

hi EndOfBuffer ctermbg=231

" Originally 153
hi CursorLine cterm=none ctermbg=7
hi CursorLinenr cterm=none

hi SpecialKey ctermbg=231
hi NonText ctermbg=231 ctermfg=250

hi Folded ctermbg=228

hi Title ctermfg=34 cterm=bold
