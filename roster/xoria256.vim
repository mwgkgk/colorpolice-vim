silent! colorscheme xoria256

hi ColorColumn ctermfg=none ctermbg=235
hi CursorLine cterm=none ctermfg=none ctermbg=235

hi EndOfBuffer ctermfg=237 ctermbg=234
hi NonText ctermfg=237 ctermbg=none

hi Todo ctermfg=247 ctermbg=none cterm=none

hi Folded ctermfg=241 ctermbg=none
hi FoldColumn ctermfg=241 ctermbg=none
hi SignColumn ctermfg=none ctermbg=none
